.. pySAXS documentation master file, created by
   sphinx-quickstart on Tue Jul 24 10:46:21 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
pySAXS 
======
Python package for Small Angle X-ray Scattering data acquisition,
treatment and computation of model SAXS intensities

:Main authors:
	Olivier Tache, Antoine Thill, Olivier Spalla,
	David Carriere, Debasis Sen, Fabienne Testard
	Laboratoire Interdisciplinaire sur l'Organisation Nanometrique et Supramoleculaire
	(L.I.O.N.S.)

:licence:
	under CECILL License (consult LICENSE.txt)
:website:
	http://www-iramis.cea.fr/sis2m/lions   
	

Welcome to pySAXS's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   modules
   pySAXS.LS.LSsca


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

