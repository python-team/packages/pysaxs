models Package
==============

:mod:`models` Package
---------------------

.. automodule:: pySAXS.models
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Capsule` Module
---------------------

.. automodule:: pySAXS.models.Capsule
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`CoreShell` Module
-----------------------

.. automodule:: pySAXS.models.CoreShell
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Doublet_Sphere` Module
----------------------------

.. automodule:: pySAXS.models.Doublet_Sphere
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Fractal` Module
---------------------

.. automodule:: pySAXS.models.Fractal
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Gaussian` Module
----------------------

.. automodule:: pySAXS.models.Gaussian
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Guinier` Module
---------------------

.. automodule:: pySAXS.models.Guinier
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`ImogoliteDW` Module
-------------------------

.. automodule:: pySAXS.models.ImogoliteDW
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`ImogoliteSW` Module
-------------------------

.. automodule:: pySAXS.models.ImogoliteSW
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`MonoCylinder` Module
--------------------------

.. automodule:: pySAXS.models.MonoCylinder
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`MonoEllipse` Module
-------------------------

.. automodule:: pySAXS.models.MonoEllipse
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`MonoSphere` Module
------------------------

.. automodule:: pySAXS.models.MonoSphere
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PCube` Module
-------------------

.. automodule:: pySAXS.models.PCube
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PCubedre` Module
----------------------

.. automodule:: pySAXS.models.PCubedre
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PCylinder_six_layers` Module
----------------------------------

.. automodule:: pySAXS.models.PCylinder_six_layers
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PCylinder_two_layers` Module
----------------------------------

.. automodule:: pySAXS.models.PCylinder_two_layers
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PParallelepiped` Module
-----------------------------

.. automodule:: pySAXS.models.PParallelepiped
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PTriedre` Module
----------------------

.. automodule:: pySAXS.models.PTriedre
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Pcylmultimin` Module
--------------------------

.. automodule:: pySAXS.models.Pcylmultimin
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PearsonVII` Module
------------------------

.. automodule:: pySAXS.models.PearsonVII
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PolyGauss` Module
-----------------------

.. automodule:: pySAXS.models.PolyGauss
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PolyGaussAnaDC` Module
----------------------------

.. automodule:: pySAXS.models.PolyGaussAnaDC
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Porod` Module
-------------------

.. automodule:: pySAXS.models.Porod
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PorodC` Module
--------------------

.. automodule:: pySAXS.models.PorodC
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PorodL` Module
--------------------

.. automodule:: pySAXS.models.PorodL
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PorodPrim` Module
-----------------------

.. automodule:: pySAXS.models.PorodPrim
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Porod_with_curvature` Module
----------------------------------

.. automodule:: pySAXS.models.Porod_with_curvature
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`ShellGaussAnaDC` Module
-----------------------------

.. automodule:: pySAXS.models.ShellGaussAnaDC
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`SphereGaussAnaDC` Module
------------------------------

.. automodule:: pySAXS.models.SphereGaussAnaDC
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Sphere_CoreShell` Module
------------------------------

.. automodule:: pySAXS.models.Sphere_CoreShell
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`SprayGrain` Module
------------------------

.. automodule:: pySAXS.models.SprayGrain
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Tetra1_Sphere` Module
---------------------------

.. automodule:: pySAXS.models.Tetra1_Sphere
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Trapez` Module
--------------------

.. automodule:: pySAXS.models.Trapez
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Triplet_Sphere` Module
----------------------------

.. automodule:: pySAXS.models.Triplet_Sphere
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`listOfModels` Module
--------------------------

.. automodule:: pySAXS.models.listOfModels
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`model` Module
-------------------

.. automodule:: pySAXS.models.model
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`pq11` Module
------------------

.. automodule:: pySAXS.models.pq11
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    pySAXS.models.super

