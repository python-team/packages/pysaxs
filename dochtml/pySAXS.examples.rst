examples Package
================

:mod:`absoluteScalingWithoutPredifinedParameters` Module
--------------------------------------------------------

.. automodule:: pySAXS.examples.absoluteScalingWithoutPredifinedParameters
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`calculateAbsorption` Module
---------------------------------

.. automodule:: pySAXS.examples.calculateAbsorption
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`calculateInvariant` Module
--------------------------------

.. automodule:: pySAXS.examples.calculateInvariant
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`importDataset` Module
---------------------------

.. automodule:: pySAXS.examples.importDataset
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`openDataAndAbsoluteScale` Module
--------------------------------------

.. automodule:: pySAXS.examples.openDataAndAbsoluteScale
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`plottingDataAndFit` Module
--------------------------------

.. automodule:: pySAXS.examples.plottingDataAndFit
    :members:
    :undoc-members:
    :show-inheritance:

