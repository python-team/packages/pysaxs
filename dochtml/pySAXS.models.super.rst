super Package
=============

:mod:`super` Package
--------------------

.. automodule:: pySAXS.models.super
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`beaucage` Module
----------------------

.. automodule:: pySAXS.models.super.beaucage
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`superModel` Module
------------------------

.. automodule:: pySAXS.models.super.superModel
    :members:
    :undoc-members:
    :show-inheritance:

