import sys
if sys.version_info.major>=3:
    from .Gaussian import *
    from .MonoSphere import *
    #from PolyGauss import *
    from .SphereGaussAnaDC import *
    from .Sphere_CoreShell import *
    from .PCube import *
    from .PCubeOT import *
    from .PParallelepiped import *
    from .MonoEllipse import *
    from .MonoCylinder import *
    from .PCylinder_two_layers import *
    from .PCylinder_six_layers import *
    from .Porod import *
    from .Porod_with_curvature import *
    from .PorodPrim import *
    from .Capsule import *
    from .StickyHS import *
    
    from .Doublet_Sphere import *
    from .Triplet_Sphere import *
    from .Tetra1_Sphere import *
    from .Guinier import *
    from .Trapez import *
    from .Fractal import *
    from .PearsonVII import *
    from .SphereGaussDoubleSizeOT import *
    from .SpherePolyLNI import *
    from .SpherePolyWBackground import *
    from .Beaucage3C import *
    from .Capillary import *
    from .SpherePolyWBC import *
    from .ImogoliteSWCH3 import *
    from .StickyHS_poly import *
    #from multiSizeOT import *
    from .Porodlimq import *

    #from superModel import *
    #from beaucage import *
    # The following models are too log to be used in guiSAXS but they are present in the package
    #from PCubedre import *
    #from PTriedre import *
    
    # The following models are too specific for a general distribution but they are present in the package
    # from SprayGrain import *
    # from SphereGaussAnaDC import *
    # from ShellGaussAnaDC import *
    #from ImogoliteDW import *
    from .ImogoliteSW import *
else:

    from Gaussian import *
    from MonoSphere import *
    #from PolyGauss import *
    from SphereGaussAnaDC import *
    from Sphere_CoreShell import *
    from PCube import *
    from PCubeOT import *
    from PParallelepiped import *
    from MonoEllipse import *
    from MonoCylinder import *
    from PCylinder_two_layers import *
    from PCylinder_six_layers import *
    from Porod import *
    from Porod_with_curvature import *
    from PorodPrim import *
    from Capsule import *
    from StickyHS import *
    
    from Doublet_Sphere import *
    from Triplet_Sphere import *
    from Tetra1_Sphere import *
    from Guinier import *
    from Trapez import *
    from Fractal import *
    from PearsonVII import *
    from SphereGaussDoubleSizeOT import *
    #from SpherePolyWBackground import *
    from Beaucage3C import *
    from Capillary import *
    from SpherePolyWBC import *
    from ImogoliteSWCH3 import *
    from StickyHS_poly import *
    #from multiSizeOT import *
    from Porodlimq import *
    
    #from superModel import *
    #from beaucage import *
    # The following models are too log to be used in guiSAXS but they are present in the package
    #from PCubedre import *
    #from PTriedre import *
    
    # The following models are too specific for a general distribution but they are present in the package
    # from SprayGrain import *
    # from SphereGaussAnaDC import *
    # from ShellGaussAnaDC import *
    #from ImogoliteDW import *
    from ImogoliteSW import *
    
